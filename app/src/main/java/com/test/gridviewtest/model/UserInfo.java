package com.test.gridviewtest.model;

/**
 * Author: dengdayi
 * Date:2016/1/11 16:04
 * Description:
 */
public class UserInfo {
    private String name;
    private int icon;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public UserInfo(String name, int icon) {
        this.name = name;
        this.icon = icon;
    }

    public UserInfo() {

    }
}
