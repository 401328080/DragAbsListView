package com.test.gridviewtest;

import android.app.Activity;
import android.os.Bundle;

import com.test.gridviewtest.adapter.ListAdapter;
import com.test.gridviewtest.view.DragListView;
import com.test.gridviewtest.view.OnChangeListener;

import java.util.List;

/**
 * Author: dengdayi
 * Date:2016/1/11 16:04
 * Description:
 */
public class DragListViewActivity extends Activity {
    public static final String TAG = "DragGridViewActivity";
    DragListView mGridView;
    ListAdapter mListAdapter;
    List<String> mStrings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_darg_list);
        mStrings = Test.getStringList();
        initView();
    }

    /**
     * 初始化数据
     */
    private void initView() {
        mGridView = (DragListView) findViewById(R.id.lv_view);
//        mGridView.setDragResponseMS(50);

        mListAdapter = new ListAdapter(this, mStrings);
        mGridView.setAdapter(mListAdapter);

        mGridView.setOnChangeListener(new OnChangeListener() {
            @Override
            public void onChange(int form, int to) {
                String tempStr = mStrings.get(form);
                mStrings.set(form, mStrings.get(to));
                mStrings.set(to, tempStr);
                mListAdapter.updateData(mStrings);
            }
        });
    }
}
