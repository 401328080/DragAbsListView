package com.test.gridviewtest.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.test.gridviewtest.R;
import com.test.gridviewtest.model.UserInfo;

import java.util.List;

/**
 * Author: dengdayi
 * Date:2016/1/11 16:04
 * Description:
 */
public class GridAdapter extends BaseAdapter {

    private List<UserInfo> mList;

    private LayoutInflater mInflater;

    public GridAdapter(Context mContext, List<UserInfo> mList) {
        this.mList = mList;
        if (mInflater == null) {
            mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder mHolder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.grid_item, null);

            mHolder = new ViewHolder();
            mHolder.mImageView = (ImageView) convertView.findViewById(R.id.iv_item);
            mHolder.mTextView = (TextView) convertView.findViewById(R.id.tv_item);

            convertView.setTag(mHolder);
        } else {
            mHolder = (ViewHolder) convertView.getTag();
        }
        //设置数据
        UserInfo mUserInfo = mList.get(position);
        if (mUserInfo != null) {
            mHolder.mTextView.setText(mUserInfo.getName());
            mHolder.mImageView.setImageResource(mUserInfo.getIcon());
        }

        return convertView;
    }

    private class ViewHolder {
        public TextView mTextView;
        public ImageView mImageView;
    }

    public void updateData(List<UserInfo> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }
}
