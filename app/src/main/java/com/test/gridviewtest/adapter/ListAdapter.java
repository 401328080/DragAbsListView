package com.test.gridviewtest.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Author: dengdayi
 * Date:2016/1/11 16:04
 * Description:
 */
public class ListAdapter extends BaseAdapter {

    private List<String> mList;

    private LayoutInflater mInflater;

    public ListAdapter(Context mContext, List<String> mList) {
        this.mList = mList;
        if (mInflater == null) {
            mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder mHolder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(android.R.layout.simple_list_item_1, null);

            mHolder = new ViewHolder();
            mHolder.mTextView = (TextView) convertView.findViewById(android.R.id.text1);

            convertView.setTag(mHolder);
        } else {
            mHolder = (ViewHolder) convertView.getTag();
        }
        //设置数据
        String str = mList.get(position);
        mHolder.mTextView.setText(str);

        return convertView;
    }

    private class ViewHolder {
        public TextView mTextView;
    }

    public void updateData(List<String> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }
}
