package com.test.gridviewtest.view;

/**
 * Author: dengdayi
 * Date:2016/1/11 16:04
 * Description:当item交换位置的时候回调的方法，我们只需要在该方法中实现数据的交换即可
 */
public interface OnChangeListener {
    /**
     * @param form 开始的position
     * @param to   拖拽到的position
     */
    void onChange(int form, int to);
}
