package com.test.gridviewtest;

import android.app.Activity;
import android.os.Bundle;

import com.test.gridviewtest.adapter.GridAdapter;
import com.test.gridviewtest.model.UserInfo;
import com.test.gridviewtest.view.DragGridView;
import com.test.gridviewtest.view.OnChangeListener;

import java.util.List;

/**
 * Author: dengdayi
 * Date:2016/1/11 16:04
 * Description:GridView排序拖动
 */
public class DragGridViewActivity extends Activity {
    public static final String TAG = "DragGridViewActivity";
    DragGridView mGridView;
    GridAdapter mGridAdapter;
    List<UserInfo> mInfos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_darg_grid);
        mInfos = Test.getUserList();
        initView();
    }

    /**
     * 初始化数据
     */
    private void initView() {
        mGridView = (DragGridView) findViewById(R.id.gv_view);
        mGridView.setDragResponseMS(50);

        mGridAdapter = new GridAdapter(this, mInfos);
        mGridView.setAdapter(mGridAdapter);

        mGridView.setOnChangeListener(new OnChangeListener() {
            @Override
            public void onChange(int form, int to) {
                UserInfo tempInfo = mInfos.get(form);
                mInfos.set(form, mInfos.get(to));
                mInfos.set(to, tempInfo);
                mGridAdapter.updateData(mInfos);
            }
        });
    }
}
