package com.test.gridviewtest;

import com.test.gridviewtest.model.UserInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: dengdayi
 * Date:2016/1/11 16:04
 * Description:
 */
public class Test {
    /**
     * 获取测试数据（字符串）
     *
     * @return
     */
    public static List<UserInfo> getUserList() {
        List<UserInfo> mStringList = new ArrayList<>();
        mStringList.add(new UserInfo("付款", R.mipmap.app_aapay));
        mStringList.add(new UserInfo("计算器", R.mipmap.app_financial));
        mStringList.add(new UserInfo("钱包", R.mipmap.app_fund));
        mStringList.add(new UserInfo("游戏充值", R.mipmap.app_game));
        mStringList.add(new UserInfo("火车票", R.mipmap.app_transfer));
        mStringList.add(new UserInfo("彩票", R.mipmap.app_lottery));
        mStringList.add(new UserInfo("手机充值", R.mipmap.app_phonecharge));
        mStringList.add(new UserInfo("电影票", R.mipmap.app_movie));
        mStringList.add(new UserInfo("滴滴出行", R.mipmap.app_citycard));
        return mStringList;
    }

    public static List<String> getStringList() {
        List<String> mStringList = new ArrayList<>();
        for (int i=1;i<=10;i++){
            mStringList.add("测试数据"+i);
        }
        return mStringList;
    }
}

